class BinaryTree:

    def __init__(self):
        self.root = None
        self.lexeme = {"+": 4, "-": 4, "*": 3, "/": 3, "l": 2, "^": 1}
        self.nonfunctional_characters = "()og_"

    def __build_tree(self, string, current_node):
        bracket_priority: int = 0
        priority_maximum = float('-inf')  # To make sure that even if the whole expression is bracketed, calculation succeeds
        maximum_position: int = 0
        for iteration in range(len(string)):
            lexeme_check = string[iteration] in self.lexeme
            opening_bracket_check = string[iteration] == ")"
            closing_bracket_check = string[iteration] == "("
            continue_values_check = string[iteration] == "o" or string[iteration] == "g" or string[iteration] == "_"
            if opening_bracket_check:
                bracket_priority += 1
            elif closing_bracket_check:
                bracket_priority -= 1
            elif lexeme_check:
                lexeme_priority = self.lexeme[string[iteration]] - 4 * bracket_priority
                check_maximum = lexeme_priority > priority_maximum
                if check_maximum:
                    maximum_position = iteration
                    check_maximum = lexeme_priority
        no_lexemes_found = priority_maximum == float('-inf')  # if priority maximum doesn't change, it means that no lexemes were found
        if no_lexemes_found:
            for character in self.nonfunctional_characters:
                string = string.replace(character, '')
            current_node.value = string
        else:
            current_node.value = string[maximum_position]
            current_node.left_child = Node()
            current_node.right_child = Node()
            self.__build_tree(string[:maximum_position], current_node.left_child)
            self.__build_tree(string[maximum_position+1:], current_node.right_child)

    def __calculate_tree(self, current_node):
        pass

    def solve(self, string):
        self.root = Node()
        self.__build_tree(string, self.root)
        self.__calculate_tree(self.root)


class Node:

    def __init__(self):
        self.value = None
        self.left_child = None
        self.right_child = None

    def is_leaf(self):
        left_child_empty = self.left_child is None
        right_child_empty = self.right_child is None
        is_leaf = left_child_empty and right_child_empty
        return is_leaf
